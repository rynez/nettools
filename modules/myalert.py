#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: myalert
# @Author: RyneZ
# @Time: 2023-01-03 9:16
from PyQt5.QtWidgets import QMessageBox


def showDialog(window, dialogtype, warnstr):
    if dialogtype == 'warning':
        QMessageBox.warning(window, '警告', warnstr)

    if dialogtype == 'information':
        QMessageBox.information(window, '信息', warnstr)