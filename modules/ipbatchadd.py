#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time : 2022/4/6 20:11
# @Author : rynez
# @File : ipbatchadd.py
import re
from IPy import IP
class ipBatchDeal():
    def __init__(self):
        #,pre_text,perobjnum,hostprefix,iscreatgroup
        # self.iplist_old=pre_text
        # self.preobjectnum=perobjnum
        # self.hostprefix=hostprefix
        # self.iscreatgroup=iscreatgroup
        #self.iplist=self.ipDeduplication(self.iplist_old)
        pass


    def ipDeduplication(self,iplist_old):
        iplist = sorted(set(iplist_old), key=iplist_old.index)  # 去除重复
        return iplist

    def dealIP(self,iplist):
        hostcontent = []  # 存放主机对象
        rangecontent = []
        subnetcontent = []
        # iphostreg=re.compile('((([01]{0,1}\d{0,1}\d|2[0-4]\d|25[0-5])\.){3}([01]{0,1}\d{0,1}\d|2[0-4]\d|25[0-5]))') #匹配单个ip地址
        # ipsubnetreg=re.compile('(((0|128|192|224|240|248|252|254)\.0\.0\.0|255\.(0|128|192|224|240|248|252|254)\.0\.0|255\.255\.(0|128|192|224|240|248|252|254)\.0|255\.255\.255\.(0|128|192|224|240|248|252|254|255)))')#匹配地址和掩码
        iphostreg = re.compile('^((\\d{1,3}\\.){3}\\d{1,3})$')
        iprangereg = re.compile('^((\d{1,3}\.){3}\d{1,3})-((\d{1,3}\.){3}\d{1,3})$')
        ipsubnetreg = re.compile(
            '^^(((\\d{1,3}\\.){3}\\d{1,3})/((\\d{1,2})$|((\\d{1,3}\\.){3}\\d{1,3})$))')
        # 统计文件中的地址、子网、个数
        for line in iplist:
            if not line.isspace():
                try:
                    line = line.strip()
                    # preipaddr=IP(line)  #先通过第三方库来判断是否为正确的地址、子网、范围对象
                    try:
                        ipaddr = iphostreg.match(line).group(1)
                        self.__dealHost(ipaddr, hostcontent)
                    except AttributeError:
                        try:
                            ipaddr = iprangereg.match(line)
                            ipaddr1 = ipaddr.group(1)
                            ipaddr2 = ipaddr.group(3)
                            self.__dealRange(ipaddr1, ipaddr2, rangecontent)
                        except AttributeError:
                            # try:
                            ipaddr = ipsubnetreg.match(line).group(1)
                            self.__dealSubnet(ipaddr, subnetcontent)
                        # except Exception as E:
                        #     print('当前行处理时出现问题：'+str(E))
                        #     ipaddr=''

                except (ValueError, AttributeError):
                    print('当前处理内容:' + line + '不是一个正确的待处理对象，请进行核对')
                    continue
        ipdict={'host':hostcontent,'range':rangecontent,'subnet':subnetcontent}
        return ipdict

    def createHostGroup(self,hostcontent,hostprefix,perobjnum,beginseq=0):
        hostgroup = {}
        # 定义主机对象资源名称等参数
        seq=beginseq
        host_count = len(hostcontent)  # host对象总数
        print('host对象共计：' + str(host_count) + '个')
        if host_count:
            # 每个主机对象包含hosts_num个主机
            hosts_pernum = int(perobjnum)
            # hosts_count 指一次生成hosts_count个主机对象
            if host_count % hosts_pernum == 0:
                hosts_count = int(host_count / hosts_pernum)
            else:
                hosts_count = int(host_count / hosts_pernum) + 1
            # 循环定义主机对象,每hosts_pernum个ip地址定义在一个主机对象中，一共循环hosts_count次
            for count in range(hosts_count):
                # 如果有输入前缀则使用前缀+序号，没有的话使用第一个对象的IP
                if hostprefix:
                    resource_name = hostprefix + str(seq)
                else:
                    resource_name = hostcontent[count * hosts_pernum]
                # 计算剩余没添加资源的IP个数
                host_last_num = host_count - hosts_pernum * count
                ipall = ''
                # 如果剩余个数大于hosts_pernum，则将hosts_pernum个IP加到一个对象中，如果小于则将剩余的所有IP加到一个对象
                for i in range(host_last_num if host_last_num <
                                                hosts_pernum else hosts_pernum):
                    ipall = ipall + hostcontent[i + count * hosts_pernum] + ' '
                # 生成单个地址对象的命令和地址组的对象

                hostgroup.update({resource_name:ipall})
                #
                # commands.append(commandline)
                #group_address.append(resource_name)
                seq = seq + 1
        return hostgroup

    def createRangeGroup(self,rangecontent,groupprefix,beginseq=0):
        rangegroup={}
        seq=beginseq
        range_count = len(rangecontent)
        print('range对象共计：' + str(range_count) + '个')
        if range_count:
            for i in range(range_count):
                if groupprefix:
                    resource_name = groupprefix + str(seq)
                else:
                    resource_name = rangecontent[i][0] + \
                                    '_' + rangecontent[i][1]
                rangegroup.update({resource_name:rangecontent[i]})
                # commandline = 'define range add name ' + resource_name + ' ip1 ' + rangecontent[i][0] + 'ip2 ' + rangecontent[i][1]
                # commands.append(commandline)
                # group_address.append(resource_name)
                seq = seq + 1
        return rangegroup

    def creatSubnetGroup(self,subnetcontent,subnetprefix,beginseq=0):
        subnetgroup={}
        seq=beginseq
        subnet_count = len(subnetcontent)
        print('subnet对象共计：' + str(subnet_count) + '个')
        if subnet_count:
            for i in range(subnet_count):
                if subnetprefix:
                    resource_name = subnetprefix + str(seq)
                else:
                    resource_name = subnetcontent[i][0] + \
                                    '_' + subnetcontent[i][1]
                subnetgroup.update({resource_name:subnetcontent[i]})
                seq = seq + 1
        return subnetgroup

    def __dealHost(self, ipstring, hostcontent):

        ipaddr = IP(ipstring)
        hostcontent.append(ipaddr.strNormal(1))

    def __dealRange(self, ipstring1, ipstring2, rangecontent):

        ipaddr1 = IP(ipstring1)
        ipaddr2 = IP(ipstring2)
        if ipaddr1 < ipaddr2:
            rangecontent.append([ipstring1, ipstring2])
        else:
            print('请检查地址范围是否正确：' + ipstring1 + '-' + ipstring2)

    def __dealSubnet(self, ipstring, subnetcontent):

        ipaddr = IP(ipstring).strNormal(2)
        subnetcontent.append(ipaddr.split('/'))

if __name__ == '__main__':
    pass
    # iplist=['1.1.1.1','1.1.1.2','1.1.1.3','10.10.0.1-10.10.0.20','192.168.1.0/24','172.16.0.0/16']
    # ip=ipBatchDeal()
    # ipdict=ip.dealIP(iplist)
    # print(ipdict)
    # hostg=ip.createHostGroup(ipdict['host'],'',2,1)
    # commands=[]
    # groupmember=[]
    # for name,addr in hostg.items():
    #     commandline = 'define host add name ' + name + ' ipaddr \'' + addr + '\''
    #     print(commandline)
    #     commands.append(commandline)
    #     groupmember.append(name)
    #
    # subnetg=ip.creatSubnetGroup(ipdict['subnet'],'',len(groupmember)+1)
    # for name,addr in subnetg.items():
    #     commandline='define range add name ' + name + ' ipaddr ' + addr[0] + ' mask ' + addr[1]
    #     commands.append(commandline)
    #     print(commandline)
    #     groupmember.append(name)
    #
    # rangeg=ip.creatSubnetGroup(ipdict['range'],'',len(groupmember)+1)
    # for name,addr in rangeg.items():
    #     commandline='define range add name ' + name + ' ipaddr ' + addr[0] + ' mask ' + addr[1]
    #     commands.append(commandline)
    #     print(commandline)
    #     groupmember.append(name)
    # print(groupmember)