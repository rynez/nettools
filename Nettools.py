#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time : 2022/4/4 21:11
# @Author : rynez
# @File : Nettools.py

from sys import exit,argv
import logging
import copy
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import re
from IPy import IP
from modules.__init__ import *
from nettoolsMainUI import *
from update import *
from regmatch import *
from duplicateremove import *
from ipjundge import *

class toolMainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        # 初始化UI
        self.setupUi(self)
        self.status = self.statusBar()
        self.status.showMessage('ver 1.0.5   by:RyneZ')
        #连接数据库
        self.dbcon=sqlite3.connect('data.db',check_same_thread=False)
        self.dbcursor = self.dbcon.cursor()
        logging.info('本地数据库连接成功')
        # 绑定槽
        ##IP批量添加
        self.ipbatch_generate_pushButton.clicked.connect(
            self.ipbatch_generate_pushButton_clicked)
        self.ipbatch_copy_pushButton.clicked.connect(
            self.ipbatch_pushButton_clicked)
        self.ipbatch_clear_pushButton.clicked.connect(
            self.ipbatch_clearButton_clicked)
        self.ipbatch_import_pushButton.clicked.connect(self.ipbatch_import_pushButton_clicked)
        self.ipbatch_output_pushButton.clicked.connect(self.ipbatch_output_pushButton_clicked)

        ##IP子网计算器
        self.ipcalc_ip1_textEdit.textChanged.connect(self.ipcalc_1_changed)
        self.ipcalc_ip2_textEdit.textChanged.connect(self.ipcalc_2_changed)
        self.ipcalc_ip3_textEdit.textChanged.connect(self.ipcalc_3_changed)
        self.ipcalc_ip4_textEdit.textChanged.connect(self.ipcalc_4_changed)
        self.ipcalc_mask_spinBox.textChanged.connect(self.maskspinbox_changed)
        self.ipcalc_mask_Slider.valueChanged.connect(self.maskslider_changed)
        self.ipcalc_calc_pushButton.clicked.connect(self.calcip)
        self.ipcalc_clearip_pushButton.clicked.connect(self.ipcalc_clean)

    def closeEvent(self, event):
        notice = QMessageBox.question(
            self,
            '提示',
            '是否要关闭所有窗口？未保存内容将会丢失。',
            QMessageBox.Yes | QMessageBox.No,
            QMessageBox.No)
        if notice == QMessageBox.Yes:
            event.accept()
            exit(0)
        else:
            event.ignore()
##槽begin##
    #IP批量生成
    def ipbatch_generate_pushButton_clicked(self):

        ipAdd=ipBatchDeal()
        iplist=ipAdd.ipDeduplication(self.ipbatch_source_textEdit.toPlainText().split())
        try:
            ipdict = ipAdd.dealIP(iplist)
        except (ValueError, AttributeError) as e:
            logging.error(str(e))
            self.logprintf(str(e))
            return -1
        commands = []
        groupmember = []
        #处理host对象
        if len(ipdict['host']):
            hostg = ipAdd.createHostGroup(ipdict['host'], self.ipbatch_hostprefix_lineEdit.text(), self.ipbatch_hostnum_lineEdit.text(), 1)
            for name, addr in hostg.items():
                commandline = 'define host add name ' + name + ' ipaddr \'' + addr + '\''
                commands.append(commandline)
                groupmember.append(name)
        #处理subnet对象
        if len(ipdict['subnet']):
            subnetg = ipAdd.creatSubnetGroup(ipdict['subnet'], self.ipbatch_hostprefix_lineEdit.text(), len(groupmember) + 1)
            for name, addr in subnetg.items():
                commandline = 'define subnet add name ' + name + ' ipaddr ' + addr[0] + ' mask ' + addr[1]
                commands.append(commandline)
                groupmember.append(name)
        #处理range对象
        if len(ipdict['range']):
            rangeg = ipAdd.creatSubnetGroup(ipdict['range'], self.ipbatch_hostprefix_lineEdit.text(), len(groupmember) + 1)
            for name, addr in rangeg.items():
                commandline = 'define range add name ' + name + ' ip1 ' + addr[0] + ' ip2 ' + addr[1]
                commands.append(commandline)
                groupmember.append(name)
        #处理地址组
        group_count = len(groupmember)  # 地址组中对象数量
        if group_count:
            if self.ipbatch_creatgroup_checkBox.isChecked():
                if group_count % 100 == 0:
                    groups_count = int(group_count / 100)
                else:
                    groups_count = int(group_count / 100) + 1  # 地址组的数量
                for count in range(groups_count):
                    if self.ipbatch_hostprefix_lineEdit.text():
                        resource_name = self.ipbatch_hostprefix_lineEdit.text() + '_group' + \
                            str(count + 1)
                    else:
                        resource_name = 'new_group' + str(count + 1)
                    group_last_num = group_count - 100 * count
                    groupall = ''
                    for i in range(
                            group_last_num if group_last_num < 100 else 100):
                        groupall = groupall + \
                            groupmember[i + count * 100] + ' '
                        #print(group_address[i + count * 100])
                    commandline = 'define group_address add name ' + \
                        resource_name + ' member \' ' + groupall + '\' '
                    commands.append(commandline)

        ipbatch_final_command = '\n'.join(commands)
        self.ipbatch_result_textEdit.setText(ipbatch_final_command)
        #光标移至最后
        cursor=self.ipbatch_result_textEdit.textCursor()
        cursor.movePosition(QTextCursor.End)
        self.ipbatch_result_textEdit.setTextCursor(cursor)
        resultstr='命令已生成，其中不重复host对象{}个，range对象{}个,subnet对象{}个'.format(
                len(ipdict['host']), len(ipdict['range']), len(ipdict['subnet']))
        self.status.showMessage(resultstr, 6000)
        self.logprintf(resultstr)

    def ipbatch_pushButton_clicked(self):

        clipboard = QApplication.clipboard()
        clipboard.setText(self.ipbatch_result_textEdit.toPlainText())
        self.status.showMessage('已成功复制', 6000)

    def ipbatch_clearButton_clicked(self):
        self.ipbatch_source_textEdit.clear()

    def ipbatch_import_pushButton_clicked(self):
        filename, _ = QFileDialog.getOpenFileName(
            self, '打开文件', '.', '所有文件(*.*)')
        try:
            with open(filename, 'r') as f:
                filelines = f.read()
            self.ipbatch_source_textEdit.setText(filelines)
        except FileNotFoundError:
            return 0
        except Exception as E:
            logging.critical('unknown error:' + str(E))
            return 0

    def ipbatch_output_pushButton_clicked(self):
        filename, _ = QFileDialog.getSaveFileName(
            self, '保存文件', '.', '所有文件(*.txt)')
        try:
            outputstr=self.ipbatch_result_textEdit.toPlainText()
            with open(filename, 'w') as f:
                f.write(outputstr)
        except Exception as E:
            logging.critical('unknown error:' + str(E))
            return 0


    def ipcalc_1_changed(self):
        try:
            if self.ipcalc_ip1_textEdit.text().endswith('.'):
                self.ipcalc_ip1_textEdit.setText(self.ipcalc_ip1_textEdit.text()[0:-1])
                self.ipcalc_ip2_textEdit.setFocus()
            if not 0 <= int(self.ipcalc_ip1_textEdit.text()) <= 255:
                self.ipcalc_ip1_textEdit.clear()
                return
        except ValueError:
            self.ipcalc_ip1_textEdit.clear()
            return
        if len(self.ipcalc_ip1_textEdit.text())==3:
            self.ipcalc_ip2_textEdit.setFocus()
    def ipcalc_2_changed(self):
        try:
            if self.ipcalc_ip2_textEdit.text().endswith('.'):
                self.ipcalc_ip2_textEdit.setText(self.ipcalc_ip2_textEdit.text()[0:-1])
                self.ipcalc_ip3_textEdit.setFocus()
            if not 0 <= int(self.ipcalc_ip2_textEdit.text()) <= 255:
                self.ipcalc_ip2_textEdit.clear()
                return
        except ValueError:
            self.ipcalc_ip2_textEdit.clear()
            return
        if len(self.ipcalc_ip2_textEdit.text())==3:
            self.ipcalc_ip3_textEdit.setFocus()
    def ipcalc_3_changed(self):
        try:
            if self.ipcalc_ip3_textEdit.text().endswith('.'):
                self.ipcalc_ip3_textEdit.setText(self.ipcalc_ip3_textEdit.text()[0:-1])
                self.ipcalc_ip4_textEdit.setFocus()
            if not 0 <= int(self.ipcalc_ip3_textEdit.text()) <= 255:
                self.ipcalc_ip3_textEdit.clear()
                return
        except ValueError:
            self.ipcalc_ip3_textEdit.clear()
            return
        if len(self.ipcalc_ip3_textEdit.text())==3:
            self.ipcalc_ip4_textEdit.setFocus()
    def ipcalc_4_changed(self):
        try:
            if not 0 <= int(self.ipcalc_ip4_textEdit.text()) <= 255:
                self.ipcalc_ip4_textEdit.clear()
                return
        except ValueError:
            self.ipcalc_ip4_textEdit.clear()
            return

    def maskspinbox_changed(self):
        self.ipcalc_mask_Slider.setValue(int(self.ipcalc_mask_spinBox.text()))
        self.calcip()

    def maskslider_changed(self):
        self.ipcalc_mask_spinBox.setValue(int(self.ipcalc_mask_Slider.value()))
        self.calcip()

    def calcip(self):
        try:
            ipaddr=self.ipcalc_ip1_textEdit.text()+'.'+self.ipcalc_ip2_textEdit.text()+'.'+self.ipcalc_ip3_textEdit.text()+'.'+self.ipcalc_ip4_textEdit.text()
            ipaddr=IP(ipaddr)
        except ValueError:
            return
        mask=IP(IP('0.0.0.0/'+self.ipcalc_mask_spinBox.text()).strNetmask())
        #网络位
        net=IP(ipaddr.int()&mask.int())
        ##计算出来的网络位/掩码
        realipaddr=IP(net.strNormal()+'/'+mask.strNormal())
        self.ipcalc_net_lineEdit.setText(net.strNormal())
        #可用地址数
        if realipaddr.prefixlen()==32:
            self.ipcalc_ipnum_lineEdit.setText('1')
        else:
            masklength=int(self.ipcalc_mask_spinBox.text())
            ipnum=2**(32-masklength)-2
            self.ipcalc_ipnum_lineEdit.setText(str(ipnum))
        # 首个可用地址
        if realipaddr.prefixlen()==31:
            self.ipcalc_firstip_lineEdit.clear()
        elif realipaddr.prefixlen()==32:
            self.ipcalc_firstip_lineEdit.setText(realipaddr.strNormal())
        else:
            firstip=IP(net.int()+1)
            self.ipcalc_firstip_lineEdit.setText(firstip.strNormal())
        #最后可用地址
        if realipaddr.prefixlen() == 31:
            self.ipcalc_lastip_lineEdit.clear()
        elif realipaddr.prefixlen()==32:
            self.ipcalc_lastip_lineEdit.setText(realipaddr.strNormal())
        else:
            lastip=IP(realipaddr.broadcast().int()-1)
            self.ipcalc_lastip_lineEdit.setText(lastip.strNormal())
        #广播地址
        self.ipcalc_broadcast_lineEdit.setText(realipaddr.broadcast().strNormal())

        #十进制掩码
        self.ipcalc_decmask_lineEdit.setText(mask.strNormal())
        #二进制掩码
        self.ipcalc_binmask_lineEdit.setText(mask.strBin())
        #二进制IP地址
        self.ipcalc_binip_lineEdit.setText(ipaddr.strBin())
        #十进制IP地址
        self.ipcalc_decip_lineEdit.setText(str(ipaddr.int()))
        #十六进制IP地址
        self.ipcalc_hexip_lineEdit.setText(ipaddr.strHex())

    def ipcalc_clean(self):
        self.ipcalc_ip1_textEdit.clear()
        self.ipcalc_ip2_textEdit.clear()
        self.ipcalc_ip3_textEdit.clear()
        self.ipcalc_ip4_textEdit.clear()
        self.ipcalc_net_lineEdit.clear()
        self.ipcalc_ipnum_lineEdit.clear()
        self.ipcalc_firstip_lineEdit.clear()
        self.ipcalc_lastip_lineEdit.clear()
        self.ipcalc_broadcast_lineEdit.clear()
        self.ipcalc_binmask_lineEdit.clear()
        self.ipcalc_decmask_lineEdit.clear()
        self.ipcalc_binip_lineEdit.clear()
        self.ipcalc_decip_lineEdit.clear()
        self.ipcalc_hexip_lineEdit.clear()
##槽end##
##功能函数begin##



    def logprintf(self, printstr):

        self.ipbatch_log_textEdit.append(printstr)  # 在指定的区域显示提示信息
        cursor = self.ipbatch_log_textEdit.textCursor()
        self.ipbatch_log_textEdit.moveCursor(cursor.End)  # 光标移到最后，这样就会自动显示出来
        QtWidgets.QApplication.processEvents()  # 一定加上这个功能，不然有卡顿


##功能函数end##
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, filename='log', format="%(asctime)s %(levelname)s：%(funcName)s：%(message)s")
    # 适应高DPI设备
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    # 适应Windows缩放
    QtGui.QGuiApplication.setAttribute(QtCore.Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    app = QApplication(argv)
    mainWindow = toolMainWindow()
    #ipjudgeconfigWidget=ipjudgeconfigWindow()
    regmatchWidget=regmatchWindow()
    duplicateremoveWidget=duplicateremoveWindow()
    ipjundgeWidget=ipjundgeWindow()
    updateForm = updateWindow()

    mainWindow.show()
    #mainWindow.ipjudge_manageIPs_pushButton.clicked.connect(lambda:ipjudgeconfigWidget.open(mainWindow.dbcursor))
    mainWindow.actionupdate.triggered.connect(updateForm.open)
    mainWindow.stools_regmatch_pushButton.clicked.connect(regmatchWidget.open)
    mainWindow.stools_duplicateremove_pushButton.clicked.connect(lambda:duplicateremoveWidget.open(mainWindow.dbcursor))
    mainWindow.stools_ipjudge_pushButton.clicked.connect(
        lambda: ipjundgeWidget.open(mainWindow.dbcursor))
    logging.info('------窗口创建成功------')
    exit(app.exec())
