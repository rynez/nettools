#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @Time : 2022/4/5 10:20
# @Author : rynez
# @File : mainWindow.py

from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIntValidator,QDoubleValidator,QRegExpValidator
from PyQt5.QtCore import QRegExp

class mainWin(QWidget):
    def __init__(self):
        super(mainWin,self).__init__()
        self.resize(300,300)
        self.setWindowTitle('test')

        formlayout=QFormLayout()
        intLineEdit=QLineEdit()
        doublelineEdit=QLineEdit()
        validEdit=QLineEdit()
        formlayout.addRow('int',intLineEdit)
        formlayout.addRow('double',doublelineEdit)
        formlayout.addRow('vali',validEdit)
        #整数校验
        intValidator=QIntValidator(self)
        intValidator.setRange(1,99)
        #浮点数校验，小数点后两位
        doubleValidator=QDoubleValidator(self)
        doubleValidator.setRange(1,100)
        doubleValidator.setNotation(QDoubleValidator.StandardNotation)
        doubleValidator.setDecimals(2)
        #正则校验器
        reg=QRegExp('[a-zA-Z0-9]+')
        validator=QRegExpValidator(self)
        validator.setRegExp(reg)

        #设置校验器
        intLineEdit.setValidator(intValidator)
        doublelineEdit.setValidator(doubleValidator)
        validEdit.setValidator(validator)



        # mainFrame=QWidget()
        self.setLayout(formlayout)
        # self.setCentralWidget(mainFrame)

    def linkHovered(self):
        print('划过标签')

    def linkClicked(self):
        print('单击')



    # def onClick_Button(self):
    #     sender=self.sender()
    #     print(sender.text()+'on')
    #     app=QApplication.instance()
    #     app.quit()

    # def center(self):
    #     screen =QDesktopWidget().screenGeometry()
    #     size=self.geometry()
    #     newLeft=(screen.width()-size.width())/2
    #     newTop=(screen.height()-size.width())/2
    #
    #     self.move(newLeft,newTop)