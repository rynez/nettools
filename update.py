#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: update
# @Author: RyneZ
# @Time: 2022-10-01 21:05


from PyQt5.QtCore import QThread, pyqtSignal
import logging
import requests
from PyQt5.QtWidgets import *
from update_ui import *


class downloadThread(QThread):
    download_proess_signal = pyqtSignal(int)

    def __init__(self, download_url, filesize, fileobj, buffer):
        super(downloadThread, self).__init__()
        self.download_url = download_url
        self.filesize = filesize
        self.fileobj = fileobj
        self.buffer = buffer

    def run(self):
        try:
            f = requests.get(self.download_url, stream=True)
            offset = 0
            for chunk in f.iter_content(chunk_size=self.buffer):

                if not chunk:
                    break
                self.fileobj.seek(offset)
                self.fileobj.write(chunk)
                offset = offset + len(chunk)
                proess = offset / int(self.filesize) * 100
                self.download_proess_signal.emit(int(proess))
            self.fileobj.close()
            self.exit(0)
        except Exception as e:
            print(e)



class updateWindow(QWidget, Ui_updateForm):
    def __init__(self):
        super(updateWindow, self).__init__()
        self.setupUi(self)
        self.guiver='105'
        self.updateprogressBar.setValue(0)
        self.downloadThread = None
        self.download_url = ''
        self.path=''
        self.newversion={}
        self.filesize = None
        self.fileobj = None
        self.auto_close = 0
        self.updateprogramButton.setEnabled(False)
        self.checknewButton.clicked.connect(self.checknew_Button_Clicked)
        self.updateprogramButton.clicked.connect(self.updateprogram_Button_Clicked)
        self.updateconfigButton.clicked.connect(self.updateconfig_Button_Clicked)
        # self.saveButton.clicked.connect(self.saveButton_Clicked)


    def open(self):
        self.show()
        self.setFixedSize(self.width(), self.height())

    def checknew_Button_Clicked(self):
        self.contentBrowser.append('正在检查更新...')

        url='https://gitlab.com/rynez/nettools/-/raw/main/dist/version.json?inline=false'
        try:
            r=requests.get(url)
        except:
            logging.error('打开网站出错，请检查网络连接')
            return 0
        contentbyte=r.content.decode()
        if contentbyte.startswith('{') and contentbyte.endswith('}'):
            self.newversion=eval(r.content.decode())
        if self.newversion:
            if int(self.guiver)<int(self.newversion['guiver']):
                self.updateprogramButton.setEnabled(True)
                self.contentBrowser.append('<font color="red">需要更新！请点击下载按钮</font>')
                self.contentBrowser.append('更新内容如下：\n'+self.newversion['whatsnew'])
            else:
                self.contentBrowser.append('<font color="blue">未发现更新版本！</font>')

    def updateprogram_Button_Clicked(self):
        self.download_url='https://gitlab.com/rynez/nettools/-/raw/main/dist/Nettools.exe'
        self.path=''.join(['Nettools v',self.newversion['guiver'],'.exe'])
        self.download()


    def updateconfig_Button_Clicked(self):
        self.download_url = 'https://gitlab.com/rynez/nettools/-/raw/main/dist/data.db'
        self.path = 'data.db'
        self.download()


    def download(self):
        self.filesize = requests.get(self.download_url, stream=True).headers['Content-Length']
        path = self.path
        self.fileobj = open(path, 'wb')
        self.downloadThread = downloadThread(self.download_url, self.filesize, self.fileobj, buffer=10240)
        self.downloadThread.download_proess_signal.connect(self.change_progressbar_value)
        self.downloadThread.start()

    def change_progressbar_value(self, value):
        self.updateprogressBar.setValue(value)
        if value==100:
            self.contentBrowser.append('更新完成')
        # if self.auto_close and value == 100:
        #     self.close()



