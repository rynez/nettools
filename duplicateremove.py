#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: duplicateremove
# @Author: RyneZ
# @Time: 2023-01-03 15:53

import logging
import re
from collections import Counter
import sqlite3
from PyQt5.QtWidgets import *
from duplicateremoveUI import *


class duplicateremoveWindow(QWidget,Ui_duplicateremove_Form):
    def __init__(self):
        super(duplicateremoveWindow, self).__init__()
        self.setupUi(self)
        self.content=''
        self.dealedcontent={}
        self.result=[]
        self.dupremove_output_pushButton.setEnabled(False)
        self.dupremove_import_pushButton.clicked.connect(self.dupremove_import_pushButton_clicked)
        self.dupremove_output_pushButton.clicked.connect(self.dupremove_output_pushButton_clicked)
        self.dupremove_check_comboBox.currentIndexChanged.connect(self.dupremove_check_comboBox_changed)
        self.dupremove_match_pushButton.clicked.connect(self.dupremove_match_pushButton_clicked)


    def open(self,dbstr):
        self.show()
        self.dbcursor = dbstr
        self.setFixedSize(self.width(), self.height())
        ##设定下拉框初始值
        self.initializesetting()

    def initializesetting(self):
        self.dbcursor.execute('select * from dupremove')
        self.regs=self.dbcursor.fetchall()
        self.dupremove_check_comboBox.addItem('')
        for reg in self.regs:
            self.dupremove_check_comboBox.addItem(reg[1])


    def dupremove_import_pushButton_clicked(self):
        filename, _ = QFileDialog.getOpenFileName(
            self, '打开文件', '.', '所有文件(*.*)')
        try:
            with open(filename, 'r') as f:
                filelines = f.read()
            self.dupremove_text_textEdit.setText(filelines)
        except FileNotFoundError as E:
            logging.critical('未找到选择的文件:' + str(E))
            return 0
        except Exception as E:
            logging.critical('unknown error:' + str(E))
            return 0

    def dupremove_output_pushButton_clicked(self):
        if self.result:
            filename, _ = QFileDialog.getSaveFileName(
                self, '保存文件', '.', '所有文件(*.txt)')
            try:
                outputstr='\n'.join(self.result)
                with open(filename, 'w') as f:
                    f.write(outputstr)
            except Exception as E:
                logging.critical('unknown error:' + str(E))
                return 0

    def dupremove_check_comboBox_changed(self):
        currentnum=self.dupremove_check_comboBox.currentIndex()
        if currentnum:
            self.dupremove_check_lineEdit.setText(self.regs[currentnum-1][2])
        else:
            self.dupremove_check_lineEdit.clear()

    def dupremove_match_pushButton_clicked(self):
        self.dupremove_output_textEdit.clear()
        self.dealedcontent={}
        self.result=[]
        self.content=self.dupremove_text_textEdit.toPlainText()
        splitsign = ' '
        #如果选择的是单行匹配
        if self.dupremove_perline_radioButton.isChecked():

            if self.dupremove_splitsign_lineEdit.text():
                splitsign = str(self.dupremove_splitsign_lineEdit.text())
            linenum=1
            for line in self.content.split('\n'):
                linelist=line.split(splitsign)
                #查找列表中重复的内容筛选出数量大于1的值
                counts = Counter(linelist)
                duplicates = {k: v for k, v in counts.items() if v > 1}
                #duplicatevalue列表为重复项中符合指定字符串类型的值
                duplicatevalue=[]
                if duplicates:
                    outputstr = '第' + str(linenum) + '行发现重复项'
                    for value,count in duplicates.items():
                        #如果勾选了校验格式
                        if self.dupremove_mulline_checkBox.checkState():
                            # 分割后的值符合指定内容才算作重复项，不符合则跳过
                            reexp=str(self.dupremove_check_lineEdit.text())
                            print(value)
                            print(re.match(reexp, value))
                            if not re.match(reexp,value):
                                continue
                        duplicatevalue.append(value)
                        outputstr=outputstr+"<span style='color: red;'>"+value+'</span>出现了'+str(count)+'次，'
                    outputstr=outputstr+'请注意。'
                    if duplicatevalue:
                        self.dupremove_output_textEdit.append(outputstr)
                #先将去重后的结果保存，如果一个值不在当前列表中或者不在重复项的列表中才会被加入此列表等待后续组合为字符串
                self.dealedcontent[linenum]=[]
                [self.dealedcontent[linenum].append(i) for i in linelist if (i not in self.dealedcontent[linenum] or i not in duplicatevalue)]
        #如果选择的是多行匹配
        elif self.dupremove_mulline_radioButton.isChecked():
            linelist=self.content.split('\n')
            positions = {}
            # i(行数)需要加一，因为默认从0开始，但是行数应从1开始
            for i, item in enumerate(linelist):
                if item in positions:
                    positions[item].append(str(i+1))
                else:
                    positions[item] = [str(i+1)]
            # 查找有重复的项的内容及其位置
            duplicates = {k: v for k, v in positions.items() if len(v) > 1}
            duplicatevalue=[]
            if duplicates:
                for value, linenumlist in duplicates.items():
                    if self.dupremove_mulline_checkBox.checkState():
                        # 分割后的值符合指定内容才算作重复项，不符合则跳过
                        reexp = str(self.dupremove_check_lineEdit.text())
                        if not re.match(reexp, value):
                            continue
                    duplicatevalue.append(value)
                    linenum=','.join(linenumlist)
                    outputstr = "发现重复项<span style='color: red;'>" + value + '</span>出现了' + str(len(linenumlist)) + '次，位置在'+linenum+'行。'
                    self.dupremove_output_textEdit.append(outputstr)
            self.dealedcontent = []
            [self.dealedcontent.append(i) for i in linelist if (i not in self.dealedcontent or i not in duplicatevalue)]

        splitstr="<span style='color: blue;'>----------------------------以下为去重后结果----------------------------</span>"
        self.dupremove_output_textEdit.append(splitstr)
        #如果self.dealedcontent为列表的话，这说明采用的是多行匹配的模式，如果是字典则是单行内匹配
        if type(self.dealedcontent)==list:
            resultstr='\n'.join(self.dealedcontent)
            self.dupremove_output_textEdit.append(resultstr)
            self.result.append(resultstr)
        elif type(self.dealedcontent)==dict:
            for linenum,content in self.dealedcontent.items():
                resultstr=splitsign.join(content)
                self.dupremove_output_textEdit.append(resultstr)
                self.result.append(resultstr)
        self.dupremove_output_pushButton.setEnabled(True)
