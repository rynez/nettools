#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: ipjundge
# @Author: RyneZ
# @Time: 2023-01-09 13:33

import copy
import re
from PyQt5.QtWidgets import *
from ipjundgeUI import *
from ipjudgeconfig import *
from modules.__init__ import *

class ipjundgeWindow(QWidget,Ui_ipjundge_Form):
    def __init__(self):
        super(ipjundgeWindow, self).__init__()
        self.setupUi(self)

        self.ipjudge_judge_pushButton.clicked.connect(self.ipjudge_judge_pushButton_clicked)
        self.ipjudge_clear_pushButton.clicked.connect(self.ipjudge_source_textEdit.clear)
        self.ipjudge_import_pushButton.clicked.connect(self.ipjudge_import_pushButton_clicked)
        self.ipjudge_output_pushButton.clicked.connect(self.ipjudge_output_pushButton_clicked)

    def open(self,dbstr):
        self.show()
        self.dbcursor = dbstr
        self.setFixedSize(self.width(), self.height())
        ipjudgeconfigWidget = ipjudgeconfigWindow()
        self.ipjudge_manageIPs_pushButton.clicked.connect(lambda: ipjudgeconfigWidget.open(self.dbcursor))

    # 判断是否包含
    def ipjudge_judge_pushButton_clicked(self):
        self.ipjudge_result_textEdit.clear()
        try:
            ipJudge = ipBatchDeal()
            iplist = ipJudge.ipDeduplication(self.ipjudge_source_textEdit.toPlainText().split())
            ipdict = ipJudge.dealIP(iplist)
        except ValueError as e:
            logging.error(str(e))
            self.resultprintf(str(e))
            return -1
        # 处理host对象
        try:
            self.dbcursor.execute('select * from ipjudge')
            self.allIPs = self.dbcursor.fetchall()
        except:
            return -1
        whiteiplist = copy.deepcopy(self.allIPs)
        iphosts = copy.deepcopy(ipdict['host'])
        deletedips = []
        for whiteip in whiteiplist:
            iprangereg = re.compile('^((\d{1,3}\.){3}\d{1,3})-((\d{1,3}\.){3}\d{1,3})$')
            iprangematch = iprangereg.match(whiteip[1])
            if len(iphosts):

                for iphost in iphosts:
                    if iprangematch:
                        ip1 = IP(iprangematch.group(1))
                        ip2 = IP(iprangematch.group(3))
                        if ip1 > ip2:
                            logging.error('地址范围' + whiteip[1] + '存在错误')

                        else:
                            if ip1.int() <= IP(iphost).int() <= ip2.int():
                                self.resultprintf(iphost + ' 存在于 ' + whiteip[1] + '，其备注为：' + whiteip[3])
                                if iphost in deletedips:
                                    continue
                                else:
                                    deletedips.append(iphost)
                                    ipdict['host'].remove(iphost)

                    else:
                        try:
                            if iphost in IP(whiteip[1] + '/' + whiteip[2]):
                                self.resultprintf(
                                    iphost + ' 存在于 ' + whiteip[1] + '/' + whiteip[2] + '，其备注为：' + str(whiteip[3]))
                                if iphost in deletedips:
                                    continue
                                else:
                                    deletedips.append(iphost)
                                    ipdict['host'].remove(iphost)
                        except ValueError:
                            self.resultprintf('地址范围：' + whiteip[1] + '/' + whiteip[2] + '异常，请检查')
                            logging.error('地址范围：' + whiteip[1] + '/' + whiteip[2] + '异常')
        self.ipjudge_outputlist = ipdict['host']

    def ipjudge_import_pushButton_clicked(self):
        filename, _ = QFileDialog.getOpenFileName(
            self, '打开文件', '.', '所有文件(*.*)')
        try:
            with open(filename, 'r') as f:
                filelines = f.read()
            self.ipjudge_source_textEdit.setText(filelines)
        except FileNotFoundError:
            return 0
        except Exception as E:
            logging.critical('unknown error:' + str(E))
            return 0

    def ipjudge_output_pushButton_clicked(self):
        filename, _ = QFileDialog.getSaveFileName(
            self, '保存文件', '.', '所有文件(*.txt)')
        try:
            outputstr = '\n'.join(self.ipjudge_outputlist)
            with open(filename, 'w') as f:
                f.write(outputstr)
        except Exception as E:
            logging.critical('unknown error:' + str(E))
            return 0

    def resultprintf(self,printstr):

        self.ipjudge_result_textEdit.append(printstr)  #在指定的区域显示提示信息
        cursor=self.ipjudge_result_textEdit.textCursor()
        self.ipjudge_result_textEdit.moveCursor(cursor.End) #光标移到最后，这样就会自动显示出来
        QtWidgets.QApplication.processEvents() #一定加上这个功能，不然有卡顿