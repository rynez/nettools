#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: ipjudgeconfig
# @Author: RyneZ
# @Time: 2022-12-20 9:37
import logging
import sqlite3
from IPy import IP
from PyQt5.QtWidgets import *
from ipjudgeconfigUI import *

class ipjudgeconfigWindow(QWidget, Ui_ipjudge_config_Form):
    def __init__(self):
        super(ipjudgeconfigWindow, self).__init__()
        self.setupUi(self)
        self.allIPs=''
        self.ipjudge_config_add_pushButton.clicked.connect(self.addButtonClickeded)
        self.ipjudge_config_del_pushButton.clicked.connect(self.delButtonClickeded)
        #self.ipjudge_config_tableWidget.clicked.connect(self.tableClicked)


    def open(self,dbstr):
        self.show()
        self.dbcursor=dbstr
        self.initializesetting()
        self.setFixedSize(self.width(), self.height())

    def initializesetting(self):
        #初始化列表，未填充数据
        header_field=['id','IP地址','掩码','备注']
        #self.dbcursor.execute('PRAGMA table_info(ipjudge)')
        #allcolumns = self.dbcursor.fetchall()
        coluncount=len(header_field)

        self.ipjudge_config_tableWidget.setColumnCount(coluncount)
        self.ipjudge_config_tableWidget.setHorizontalHeaderLabels(header_field)
        self.ipjudge_config_tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.ipjudge_config_tableWidget.horizontalHeader().setSectionResizeMode(1, QHeaderView.Interactive)
        self.ipjudge_config_tableWidget.horizontalHeader().setSectionResizeMode(2, QHeaderView.Interactive)
        self.ipjudge_config_tableWidget.horizontalHeader().setSectionResizeMode(0, QHeaderView.Fixed)#第0列固定
        self.ipjudge_config_tableWidget.setColumnWidth(0, 60)  # 设置第0列宽度
        self.ipjudge_config_tableWidget.setColumnHidden(0, True);
        self.setTableContents()


    def setTableContents(self):
        self.dbcursor.execute('select * from ipjudge')
        self.allIPs = self.dbcursor.fetchall()
        rowcount = len(self.allIPs)
        self.seqdict={}
        self.ipjudge_config_tableWidget.setRowCount(rowcount)
        i=0
        for ip in self.allIPs:
            self.ipjudge_config_tableWidget.setItem(i, 0, QTableWidgetItem(str(ip[0])))
            self.ipjudge_config_tableWidget.setItem(i, 1, QTableWidgetItem(str(ip[1])))
            self.ipjudge_config_tableWidget.setItem(i, 2, QTableWidgetItem(str(ip[2])))
            self.ipjudge_config_tableWidget.setItem(i, 3, QTableWidgetItem(str('无' if ip[3]=='' else ip[3])))
            i=i+1

    def addButtonClickeded(self):

        ipaddr=str(self.ipjudge_config_ipaddr_lineEdit.text())
        mask=str(self.ipjudge_config_mask_lineEdit.text())
        if len(ipaddr.split('-'))==2:
            ip1,ip2=ipaddr.split('-')
            try:
                IP(ip1)
                IP(ip2)
            except:
                logging.error('错误的地址:' + ipaddr )
        elif len(ipaddr.split('-'))==1:
            #先判断IP和掩码是否为正确值
            try:
                tempip=(IP(ipaddr).int())
                tempmask=(IP(IP('0.0.0.0/'+mask).strNetmask()).int())
            except ValueError:
                logging.error('错误的地址或掩码:'+ipaddr+'/'+mask)
                return 0
            #由于IPy库无法使用192.168.1.1/24，只能使用192.168.0.0/24，为了后续调用更方便，在存入数据库前先将地址换成网络位
            try:
                IP(ipaddr)
                IP(ipaddr+'/'+mask)
            except ValueError:
                ipresult=tempip&tempmask
                ipaddr=(IP(ipresult).strNormal(1))
        note=str(self.ipjudge_config_note_lineEdit.text())
        sql="insert into ipjudge (ipaddr,mask,note) values (\'"+ipaddr+"\',\'"+mask+"\',\'"+note+"\');"
        self.dbcursor.execute(sql)
        self.setTableContents()
        self.dbcursor.execute('commit;')

    def delButtonClickeded(self):
        try:
            rownum=self.ipjudge_config_tableWidget.selectedItems()[0].row()
            addrid=self.ipjudge_config_tableWidget.item(rownum,0).text()
            sql="delete from ipjudge where id ="+addrid+";"
            self.dbcursor.execute(sql)
            self.setTableContents()
            self.dbcursor.execute('commit;')
        except IndexError:
            print('请选择待删除项')