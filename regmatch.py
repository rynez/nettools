#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: regmatch
# @Author: RyneZ
# @Time: 2022-12-30 11:16
import logging
import re

from PyQt5.QtGui import QTextCursor, QBrush, QColor, QTextCharFormat

from regmatchUI import *
from PyQt5.QtWidgets import *
from modules.__init__ import *


class regmatchWindow(QWidget, Ui_regmatch_Form):
    def __init__(self):
        super(regmatchWindow, self).__init__()
        self.setupUi(self)
        self.dstposition=[]

        self.regmatch_import_pushButton.clicked.connect(self.regmatch_import_pushButton_clicked)
        self.regmatch_search_pushButton.clicked.connect(self.regmatch_search_pushButton_clicked)
        self.regmatch_matchnext_pushButton.clicked.connect(self.regmatch_matchnext_pushButton_clicked)
        self.regmatch_replaceonce_pushButton.clicked.connect(self.regmatch_replaceonce_pushButton_clicked)
        self.regmatch_replaceall_pushButton.clicked.connect(self.regmatch_replaceall_pushButton_clicked)

    def open(self):
        self.show()
        self.setFixedSize(self.width(), self.height())


    def regmatch_import_pushButton_clicked(self):
        filename, _ = QFileDialog.getOpenFileName(
            self, '打开文件', '.', '所有文件(*.*)')
        try:
            with open(filename, 'r') as f:
                filelines = f.read()
            self.regmatch_text_textEdit.setText(filelines)
        except FileNotFoundError as E:
            logging.critical('未找到选择的文件:' + str(E))
            return 0
        except Exception as E:
            logging.critical('unknown error:' + str(E))
            return 0
        self.textcursor = self.regmatch_text_textEdit.textCursor()
        #初始化文本框
        self.allcontentblack()
        self.textcursor.setPosition(0)

    def regmatch_search_pushButton_clicked(self):
        self.textcursor = self.regmatch_text_textEdit.textCursor()
        self.searchandtagred(0)

    def regmatch_matchnext_pushButton_clicked(self):
        ###思路为：根据当前鼠标所在定位，在所有匹配位置定位的列表中找到其后个匹配的位置，并根据长度选中
        self.textcursor = self.regmatch_text_textEdit.textCursor()
        print(self.textcursor.position())
        self.searchandtagred(self.textcursor.position())

    def regmatch_replaceonce_pushButton_clicked(self):
        self.textcursor = self.regmatch_text_textEdit.textCursor()
        #如果当前选择的有内容，则替换，如果没有选择则先查找下一个再替换
        if self.textcursor.selectedText():
            newstr=self.getnewcontent(self.textcursor.selectedText(),self.regmatch_regexp_lineEdit.text(),str(self.regmatch_replacereg_lineEdit.text()))
            if newstr:
                self.textcursor.insertText(newstr)
                self.regmatch_matchnext_pushButton_clicked()
        else:
            self.regmatch_matchnext_pushButton_clicked()
            newstr = self.getnewcontent(self.textcursor.selectedText(), self.regmatch_regexp_lineEdit.text(),str(self.regmatch_replacereg_lineEdit.text()))
            if newstr:
                self.textcursor.insertText(newstr)
                self.regmatch_matchnext_pushButton_clicked()

    def regmatch_replaceall_pushButton_clicked(self):
        self.regmatch_search_pushButton_clicked()
        newstr = self.getnewcontent(self.textcursor.selectedText(), self.regmatch_regexp_lineEdit.text(),str(self.regmatch_replacereg_lineEdit.text()))
        if newstr:
            replacedcontent=re.sub(self.regmatch_regexp_lineEdit.text(),newstr,self.regmatch_text_textEdit.toPlainText())
            self.allcontentblack()
            self.regmatch_text_textEdit.setText(replacedcontent)



#####
    def getnewcontent(self,contentstr,regstr,expectstr):
        #先判断选中的内容中是否包含符合正则的内容
        try:
            regex=re.compile('('+regstr+')')
            rematch=regex.match(contentstr)
            #regmatch=re.findall(contentstr,regstr)
            regroups = rematch.groups()
        except re.error:
            showDialog(self, 'warning', "请检查输入内容是否正确，例如如果需要匹配单斜杠'\\'，请输入两个斜杠'\\\\'")
            logging.error('正则表达式中输入了无法处理的内容：' + regstr)
            return 0
        except AttributeError:
            showDialog(self, 'warning', "当前选中内容不符合表达式，请以查找功能的选定结果为准。\n如果需要匹配更多的内容，请灵活在正则表达式前后使用‘.*’等内容进行匹配")
            return 0
        #根据输入生成需要替换为的内容
        seqreg='\\\\[1-9]'
        seqlist=re.findall(seqreg,expectstr)
        newlist=re.split('(\\\\[1-9])',expectstr)
        try:
            for seqstr in seqlist:
                newstr=regroups[int(seqstr[-1:])]
                newlist[newlist.index(seqstr)]=newstr
        except IndexError:
            showDialog(self, 'warning', "未选择需要替换的文本或者当前输入内容中无相关序号匹配的内容，例如只有两个()选定了内容，但在替换文本中使用了\\3")
            return 0

        return ''.join(newlist)


    def allcontentblack(self):
        format = QTextCharFormat()
        format.setForeground(QBrush(QColor(0, 0, 0)))
        self.textcursor.setPosition(0)
        self.textcursor.setPosition(len(self.regmatch_text_textEdit.toPlainText()), QtGui.QTextCursor.KeepAnchor)
        self.textcursor.setCharFormat(format)

    def searchandtagred(self,firstpositon):
        #将匹配的内容在文本中的定位position开始位置及结束位置放到dstposition中，[[start,end],[start,end],...]
        self.dstposition = []
        regstr = str(self.regmatch_regexp_lineEdit.text())
        #开始查找的位置，点击查找传入的是0，点击查找下一个传入的是当前光标的position
        startposition = firstpositon
        self.allcontentblack()
        # 获取文档对象
        document = self.regmatch_text_textEdit.document()
        # 获取行数
        line_count = document.blockCount()
        perlinenumdict = {}
        postionnow = 0
        # 遍历每一行
        for i in range(line_count):
            # 获取当前行的内容
            block = document.findBlockByNumber(i)
            line = block.text()
            # perlinenumdict[i] = len(line)
            # print(line)
            try:
                regroups = re.finditer(regstr, line)
            except re.error:
                showDialog(self,'warning',"请检查输入内容是否正确，例如如果需要匹配单斜杠'\\'，请输入两个斜杠'\\\\'")
                logging.error('正则表达式中输入了无法处理的内容：'+regstr)
                return -1
            if not regroups:
                postionnow = postionnow + len(line) + 1
            else:
                for match in regroups:
                    # 获取匹配的子字符串
                    sub_str = match.group()
                    # 获取匹配的子字符串的起始位置
                    start = match.start()
                    # 获取匹配的子字符串的结束位置
                    end = match.end()
                    # 当 当前匹配到的位置在光标所在位置之后才会添加到列表中
                    if postionnow + start >= startposition:
                        self.dstposition.append([postionnow + start, postionnow + end])
                postionnow = postionnow + len(line) + 1

        ##对命中的字符串更换颜色

        # 将匹配到的字符都转化为红色
        for position in self.dstposition:
            self.textcursor.setPosition(position[0])
            self.textcursor.setPosition(position[1], QtGui.QTextCursor.KeepAnchor)
            text_format = QTextCharFormat()
            text_format.setForeground(QBrush(QColor('red')))
            # self.regmatch_text_textEdit.setTextCursor(textcursor)
            self.textcursor.mergeCharFormat(text_format)
        self.regmatch_text_textEdit.setFocus()
        # 选中第一个匹配的字符串
        try:
            self.textcursor.setPosition(self.dstposition[0][0])
            self.textcursor.setPosition(self.dstposition[0][1], QtGui.QTextCursor.KeepAnchor)
        except IndexError:
            return
        self.regmatch_text_textEdit.setTextCursor(self.textcursor)
